extends KinematicBody2D

export var move_speed = Vector2(6, 0)
export var gravity = Vector2(0, 6)
export var jump_speed = Vector2(0, -100)
var floor_normal = Vector2(0, -1)
var movement_velocity

onready var sprite = get_node("Sprite")
var current_frame = 0 # The frame we are currently on
export var move_right_start_frame = 0 # The first frame of the right movement animation cycle
export var move_left_start_frame = 4 # The first frame of the left movement animation cycle
export var frames_per_animation = 4 # The number of frames in a full animation cycle

export var animations_per_second = 2
export var physics_cycles_per_second = 60
var cycles_per_animation_frame # physics cycles between new animation frames
var cycles_since_last_animation_frame # physics cycles since the last new animation frame

# Called when the node enters the scene tree for the first time.
func _ready():
	movement_velocity = Vector2(0, 0)
	
	var frames_per_second = animations_per_second * frames_per_animation
	cycles_per_animation_frame = physics_cycles_per_second / frames_per_second
	
	cycles_since_last_animation_frame = 0

func _physics_process(delta):
	movement_velocity.x = 0
	_move_gravity()
	_move_left()
	_move_right()
	_move_jump()
	_move()
	_animate()
		
func _move_left():
	if Input.is_action_pressed("ui_left"):
		movement_velocity = movement_velocity - move_speed
		
func _move_right():
	if Input.is_action_pressed("ui_right"):
		movement_velocity = movement_velocity + move_speed

func _move_jump():
	if Input.is_action_just_pressed("ui_up") and is_on_floor():
		movement_velocity = movement_velocity + jump_speed

func _move_gravity():
	if is_on_floor():
		movement_velocity.y = gravity.y
	else:
		movement_velocity = movement_velocity + gravity
	
func _move():
	move_and_slide(movement_velocity, floor_normal)
	
# Animations

func _animate():
	cycles_since_last_animation_frame = cycles_since_last_animation_frame + 1
	if cycles_since_last_animation_frame >= cycles_per_animation_frame:
		current_frame = (current_frame + 1) % frames_per_animation
		# 0 % 4 = 0
		# 1 % 4 = 1
		# 2 % 4 = 2
		# 3 % 4 = 3
		# 4 % 4 = 0
		if movement_velocity.x > 0:
			sprite.frame = current_frame + move_right_start_frame
		if movement_velocity.x < 0:
			sprite.frame = current_frame + move_left_start_frame
		cycles_since_last_animation_frame = 0













