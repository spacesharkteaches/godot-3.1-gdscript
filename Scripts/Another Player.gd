extends KinematicBody2D

export var move_speed = Vector2(500, 0)
export var gravity = Vector2(0, 50)
export var jump_speed = Vector2(0, -1000)
var floor_normal = Vector2(0, -1)
var movement_velocity

onready var animated_sprite = get_node("Animated Sprite")
var moving_right

# Called when the node enters the scene tree for the first time.
func _ready():
	movement_velocity = Vector2(0, 0)
	moving_right = true

func _physics_process(delta):
	movement_velocity.x = 0
	_move_gravity()
	_move_left()
	_move_right()
	_move_jump()
	_move()
	_animate()
		
func _move_left():
	if Input.is_action_pressed("ui_left"):
		movement_velocity = movement_velocity - move_speed
		
func _move_right():
	if Input.is_action_pressed("ui_right"):
		movement_velocity = movement_velocity + move_speed

func _move_jump():
	if Input.is_action_just_pressed("ui_up") and is_on_floor():
		movement_velocity = movement_velocity + jump_speed

func _move_gravity():
	if is_on_floor():
		movement_velocity.y = gravity.y
	else:
		movement_velocity = movement_velocity + gravity
	
func _move():
	move_and_slide(movement_velocity, floor_normal)
	
# Animations

func _animate():
	var moving = false
	
	if movement_velocity.x > 0:
		animated_sprite.flip_h = false
		moving = true
	elif movement_velocity.x < 0:
		animated_sprite.flip_h = true
		moving = true
	else:
		moving = false
		
	if moving:
		animated_sprite.play("Moving")
	else:
		animated_sprite.play("Idle")
#	animated_sprite.flip_h = false
#	if movement_velocity.x > 0:
#		animated_sprite.play("Move Right")
#		moving_right = true
#	elif movement_velocity.x < 0:
#		animated_sprite.play("Move Left")
#		moving_right = false
#	else:
#		animated_sprite.play("Idle")
#		animated_sprite.flip_h = !moving_right













