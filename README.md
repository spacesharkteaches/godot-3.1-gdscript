# Space Shark Teaches Godot 3.1 GDScript

Welcome to the Space Shark Teaches Godot 3.1 GDScript git repo!

This repository contains the most up-to-date version of the Godot 3.1 GDScript tutorial.

Please see [Tags](https://gitlab.com/spacesharkteaches/godot-3.1-gdscript/-/tags) to the version of the project that best lines up with your place in the lessons.

### [Space Shark Studios on YouTube](https://www.youtube.com/c/SpaceSharkStudios)

### [Godot 3.1 GDScript YouTube Playlist](https://www.youtube.com/playlist?list=PLxw4Pmjew9pwN0zkfv40DSr4Vf72giPcm)